package com.doacaodemedicamentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoacaoDeMedicamentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoacaoDeMedicamentosApplication.class, args);
	}

}
